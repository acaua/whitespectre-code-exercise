import init from '../modules/init';

class App {
	constructor() {
		this.id = Date.now();
	}

	init(callback) {
		init.call(this, callback);
	}
}

export default new App();
