import pug from 'pug';
import app from '../app/index';
import CarouselCollection from '../collections/carousel';

const carouselCollection = new CarouselCollection();

export default Backbone.View.extend({
	el: '#app',

	formatData: function (results, id) {
		if (id > results.length) {
			app.router._default();
			return;
		}

		return {
			data: results[id - 1],
			hasPrev: !(id > 1),
			hasNext: !(id < results.length)
		};
	},

	fetch: function (callback) {
		carouselCollection.fetch({
			success: (meta, res) => {
				callback.call(this, res.results);
			}
		});
	},

	render: function (id) {
		this.fetch(results => {
			$.get('./templates/carousel.pug', data => {
				this.el.innerHTML = pug.compile(data)(this.formatData(results, id));
			});
		});
	}
});
