import app from '../app/index';

export default Backbone.Router.extend({
	routes: {
		':id': 'query',
		'*default': '_default'
	},

	query: function (id) {
		if (id < 1) {
			this._default();
			return;
		}

		app.view.render(id);
	},

	_default: function () {
		this.navigate('1', {trigger: true});
	}
});
