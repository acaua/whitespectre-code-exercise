import CarouselModel from '../models/carousel';

export default Backbone.Collection.extend({
	url: './content.json',
	model: CarouselModel
});
