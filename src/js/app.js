console.time('Initialize');
import app from './app/index';
import CarouselView from './views/carousel';
import CarouselRouter from './routers/carousel';


app.init(function () {
	app.view = new CarouselView();
	app.router = new CarouselRouter();

	Backbone.history.start();

	console.log('%cMantis Starter', 'color: #338656; font: 50px sans-serif;');
	console.timeEnd('Initialize');
});
