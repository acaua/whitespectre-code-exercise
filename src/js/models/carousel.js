export default Backbone.Model.extend({
	defaults: {
		id: null,
		title: null,
		images: null
	}
});
